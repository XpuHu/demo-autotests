const data = require('./person.json');

const { licensePlate } = data.carData;
const number = licensePlate.substr(0, 6);
const region = licensePlate.substr(6, licensePlate.lenght);

Feature('Page 1, License');

Scenario('Page with license is opened', ({ I }) => {
    I.seeElement('.license__plate');
    I.seeInCurrentUrl('/');
});

Scenario('Empty fields', ({ I }) => {
    function submitFail() {
        I.click('//button[@type="submit"]');
        I.see('Укажите номер автомобиля');
    }

    // Empty region
    I.fillField('#number', number);
    submitFail();

    // Empty number and region
    I.clearField('#number');
    submitFail();

    // Incorrect number
    I.fillField('#number', number.slice(0, 5));
    I.fillField('#region', region);
    submitFail();
});

Scenario('Check validation', ({ I }) => {

    // Incorrect russian letters in number

    // Incorrect english letters in number

    // Mask in number (Used Unicode code instead "У")

    // Invalid symbols in region

    // Transform english letter: F -> A

    // Transform english letter: D -> В

    // Transform english letter: T -> Е

    // Transform english letter: V -> м

    // Transform english letter: Y -> Н

    // Transform english letter: J -> О

    // Transform english letter: H -> Р

    // Transform english letter: C -> C

    // Transform english letter: N -> Т

    // Transform english letter: E -> У

    // Transform symbol: { -> Х

    // Transform symbol: [ -> Х

});

// Scenario('Page 1, license', ({ I }) => {
//     I.fillField('#number', 'Х031ВУ197');
    
//     I.seeInField('#number', 'Х031ВУ');
//     I.seeInField('#region', '197');

//     I.click('//button[@type="submit"]');
//     I.seeInCurrentUrl('/info/car');
//     I.seeElement('//section[@class="vendors"]');
// });