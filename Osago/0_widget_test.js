Feature('Osago widget');

Before(({ I }) => {
    I.amOnPage('https://test-osago.insapp.ru/');
});

Scenario('Open Osago widget', ({ I }) => {
    I.seeElement('//section[@class="license"]');
    I.seeElement('.license__plate');
});
